title: What is Continuous Integration (CI)?
seo_title: What is Continuous Integration (CI)?
description: Learn about continuous integration and its benefits and see why
  organizations that use continuous integration have a competitive advantage
  over those that don’t.
header_body: Learn about continuous integration and its benefits and see why
  organizations that use continuous integration have a competitive advantage
  over those that don’t.
canonical_path: /topics/ci-cd/benefits-continuous-integration/
file_name: benefits-continuous-integration
parent_topic: ci-cd
twitter_image: /images/opengraph/ci-cd-opengraph.png
cover_image: /images/topics/g_gitlab-ci-cd.svg
body: >-
  ## Continuous Integration (CI)


  Continuous integration (CI) is a development practice whereby developers regularly merge their code changes into a shared repository, typically multiple times per day. By doing so, they can detect and resolve conflicts early, avoid having to coordinate large-scale code changes, and ensure that the codebase remains stable.

  By integrating regularly, developers can detect errors quickly, and locate them more easily. CI is a key practice for Agile development teams. 


  ## What are the benefits of continuous integration?

  [Continuous integration (CI)](https://about.gitlab.com/topics/ci-cd/) makes software development easier, faster, and less risky for developers. By automating builds and tests, developers can make smaller changes and commit them with confidence. Developers get feedback on their code sooner, increasing the overall pace of innovation.


  Organizations that adopt continuous integration have [a competitive advantage](https://about.gitlab.com/blog/2019/06/27/positive-outcomes-ci-cd/) because of the ability to deploy faster. Organizations that have implemented CI are making revenue on the features they deploy, not waiting for manual code checks.


  Studies done by DevOps Research and Assessment (DORA) have shown that robust DevOps practices lead to improved [business outcomes](https://cloud.google.com/devops/state-of-devops/). All of these DORA 4 metrics can be improved by using CI:


  * **Lead time**: Early feedback and build/test automation help decrease the time it takes to go from code committed to code successfully running in production.

  * **Deployment frequency**: Automated tests and builds are a pre-requisite to automated deploy.

  * **Time to restore service**: Automated pipelines enable fixes to be deployed to production faster reducing Mean Time to Resolution (MTTR).

  * **Change failure rate**: Early automated testing greatly reduced the number of defects that make their way out to production.


  ## How do I set up continuous integration?

  If you want to set up CI, there are a few things you need to do. First, you need to choose a continuous integration server. There are many options available, so you need to decide which one is right for your project. Once you have chosen a server, you need to set up your project on the server. This usually involves creating a new project on the server and then adding your code to the project. Once your project is set up, you then need to configure your build.


  ## The business benefits of continuous integration

  With less manual work, DevOps teams work more efficiently and with greater speed. An automated workflow also improves handoffs, which enhances overall operational efficiency. The [business benefits of continuous integration](/stages-devops-lifecycle/continuous-integration/) allow organizations to:


  * **Iterate faster**: Smaller code changes allow teams to iterate faster and are easier to manage.

  * **Find problems easily**: Teams can find problems in code because all code is managed and tested in smaller batches.

  * **Have better transparency**: Continuous feedback through frequent testing helps developers see where bugs are.

  * **Reduce costs**: Automated testing frees up time for developers by reducing manual tasks, and better code quality means fewer errors and less downtime.

  * **Make users happy**: Fewer bugs/errors make it into production, so users have a better experience.


  Automated testing reduces the chances of human error and ensures that only code that meets certain standards makes it into production. Because code is tested in smaller batches, there’s less context-switching for developers when a bug/error occurs. Pipelines can also identify *where* the error occurs, making it easier to not only identify problems, but fix them.


  A dev environment with less manual tasks means that engineers can spend more time on revenue-generating projects. With fewer errors, teams are more efficient and spend less time putting out fires. When processes, such as unit testing, are automated, engineers are happier and can focus on where they add the most value.


  ## What is the difference between continuous integration (CI) and continuous delivery (CD)?

  Continuous integration and continuous delivery (CI/CD) bring [automation into the DevOps lifecycle](https://about.gitlab.com/blog/2019/04/25/5-teams-that-made-the-switch-to-gitlab-ci-cd/). DevOps teams work more efficiently and with greater speed with minimal manual work. An automated workflow also reduces the chances of human error and improves handoffs, which increases overall operational efficiency. Organizations that implement CI/CD make better use of their resources, are more cost efficient, and allow developers to focus on innovation.


  There are a few key differences between continuous integration and continuous delivery. For one, continuous integration generally happens more frequently than continuous delivery. Continuous integration is also typically used to refer to the process of automating the build and testing of code changes, while continuous delivery generally refers to the process of automating the release of code changes.


  * **Continuous integration (CI)** is the practice of merging all developer working copies to shared mainline several times a day.

  * **Continuous delivery (CD)** is a software development practice where code changes are automatically built, tested, and deployed to production.
  

cta_banner:
  - cta:
      - url: /stages-devops-lifecycle/continuous-integration/
        text: Learn More
    title: Continuous Integration (CI) with GitLab
    body: Powerful automation to build and test faster at any scale
resources_title: More on continuous integration
resources:
  - url: https://about.gitlab.com/customers/hotjar/
    type: Case studies
    title: How Hotjar deploys 50% faster with GitLab
  - url: /resources/ebook-single-app-cicd/
    type: Books
    title: The benefits of a single application CI/CD
