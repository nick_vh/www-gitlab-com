# Channel Partners: Working with GitLab

GitLab Channel Partners are the primary audience for this handbook page.

**We invite organizations interested in becoming a GitLab Channel Partner to [APPLY NOW](https://partners.gitlab.com/English/register_email.aspx).**
Onboarding partners is incredibly important to us at GitLab. We want to ensure that you quickly get the training and resources you need to get off to a strong start with GitLab.

For an overview of the GitLab reseller program, [click here](https://about.gitlab.com/handbook/resellers/). 
In this section, we cover:

[[_TOC_]]

## Partner Registration: Getting Started with GitLab

To get started with GitLab, register your company by going to [https://partners.gitlab.com](https://partners.gitlab.com). The system will prompt you for your company and personal contact information, as well as your personal contact information. A GitLab Channel Account Manager may contact you for qualification. If you agree to proceed, your company will be required to sign a GitLab Reseller Agreement. Once you are registered, you can:
* Work with your GitLab Channel Account Manager or distributor to begin business and sales planning.
* Visit the portal and encourage your colleagues to sign up at [https://partners.gitlab.com](https://partners.gitlab.com).
* Complete the onboarding tasks that will help you familiarize yourself with available GitLab resources.
* Take the sales training as well as other relevant training.
* Register your first deal.

## Completing Training and Certifications

Our training programs are available primarily through the [GitLab Partner Portal](https://partners.gitlab.com/). There, you will find the certification courses and testing needed to meet your GitLab program requirements for sales, pre-sales, technical, and professional services training.

## Deal Registration Program Overview

To promote the growth and success of GitLab channels, manage channel conflict, and provide a systematic approach to pursuing sales opportunities, GitLab has formalized the rules of engagement and deal registration procedures. Since the first qualified deal registration requires approval, partners are encouraged to submit their deal registrations as early as possible to lock in the biggest discount. Partners in the GitLab Partner Program need to register individual sales opportunities; partners may not register an account or combine opportunities.

* Deal registration approval is based upon order of receipt of the registration, qualification of the opportunity, partner ability to deliver in-country/region support, and partner relationship with the customer. Deal registration approval is required and will be made by GitLab’s Sales team.
* Only one partner can earn a deal registration discount per opportunity. Partners, other than the partner granted the deal registration discount that requests a quote, will receive the co-sell discount rate.
* New or existing customer opportunities or new opportunities with existing customers can qualify for deal registration discounts. Add-on sales to renewals can also qualify.
* Approved deal registrations have a standard 90-day expiration from the date of original approval. (Deal Registration extensions beyond the initial 90 days approval are at the sole discretion of GitLab.)
GitLab collaborates with partners holding the approved deal registration and is available to support partners throughout the entire sales process.
* In the event the engagement is dissolved, the GitLab Sales Rep will generally notify you by phone or email. GitLab will reconsider other deal registrations submitted for this deal, in chronological order of submission. If there are no other registration requests submitted, the GitLab Sales Rep will typically initiate engagement with a reseller of the GitLab Sales Rep’s choosing.

Only authorized partners that have signed the GitLab Partner Program Agreement are able to register deals, earn program discounts, and transact with GitLab. Unauthorized partners do not qualify. Additionally, at least one of the partner’s employees must complete the Sales Core training for the partner to qualify for deal registration and program discounts.

### Deal Registration Instructions
Our Deal Registration process allows us to incentivize our partners to source and close GitLab products and services opportunities and avoid channel conflict. You can find an overview of the Deal Registration Program, including rules of engagement, here. The following section provides instructions for this process.

### Submitting a Deal Registration
1. Go to the [GitLab Partner Portal](https://partners.gitlab.com/) at partners.gitlab.com and select “Register a Deal.”
2. Choose to "Add a Registration."
3. Choose "GitLab" under the list of vendors.
4. When asked to select a program, choose either "Deal Registration" for a standard licensing sale (“Referral,” “Resale,” or “MSP”) or "Service Attached Registration" if you're submitting a registration to participate in our rebate program for services that you're providing to the customer.
5. Complete and ensure all information has been filled out. GitLab will not engage based on a speculative or poorly defined opportunity or with a reseller that is not eligible to win a sales opportunity.
6. Choose “Submit the deal.”
7. Go through the review and approval process.

Once deal registrations are received, a GitLab Sales Rep will:
* Review the deal registration application.
* Advise if you are the first to register an opportunity.
* Work with you to confirm whether a deal is a qualified sales opportunity.

### How Licensing Registrations Work

* Only a properly completed deal registration submission can initiate an engagement and, as mentioned, engagements are not exclusive. Thus, it is in all parties’ best interest to complete a deal registration form as soon as a qualified sales opportunity is identified.
* The GitLab Sales Rep checks to see if GitLab is already engaged with another reseller on this opportunity. If so, the Sales Rep rejects the deal registration and notifies you that GitLab cannot engage with you.
* If GitLab is not already engaged with another reseller, the Sales Rep or Account Executive contacts you to schedule a meeting or conference call with the customer to confirm that the following requirements are met:
    * The information submitted on the deal registration form is complete and correct.
    * A qualified sales opportunity exists.
    * You are in full compliance with an appropriate and effective GitLab Reseller Agreement and all applicable policies/programs.
    * The customer has not selected an alternate GitLab reseller for this deal.
    * The customer agrees that you are eligible to win this business.

Note: “Eligible” means that you meet all contractual or regulatory requirements to bid on the deal and that the customer is willing to buy you. This is particularly relevant with government contracts or bids, but could also apply to commercial work.

If any of the above requirements are not met—or you fail to promptly schedule a meeting or conference call with the customer—the GitLab Sales Rep may advise you that GitLab cannot engage with you for this opportunity. GitLab will then reconsider other deal registration applications submitted for this deal in order of submission. You may subsequently attempt to remedy this situation and submit another deal registration form for this deal, but this will be considered a new submission.

## How Services Attached Registrations Work

The Services Attached incentive is based on the volume of licensing opportunities with services attached to those licensing deals. The more licensing deals you attach services to the richer the program becomes.
* Services Attach deal registrations are submitted via the Partner Portal as a deal registration (Deal Registration Type = Services Attach) and then linked to GitLab licensing opportunities.
* In order to receive their rebate, partners must submit reasonable information and documentation proving that the services meet both program guidelines and customer needs.
* There are two (2) parts to this process:
    1. Partners need to email proof of execution (POE) for delivery of services to the customer. The POE must include customer confirmation of the services and that the services meet/met the guidelines as outlined in the current program guidance (found in the Partner Portal) for the incentive. The most likely form of POE is a signed statement of work (SOW) outlining work and cost to the customer. POEs must be approved by GitLab Sales.
        * Email POE to partnersupport@gitlab.com.
    2. Partners need to submit the [Proof of Execution (POE) form](https://forms.gle/n8xDfkNbbeNKwaG36).

Rebates are paid out no later than 45 days after the end of each quarter.  Partners need to have connected with GitLab in our billing system,Coupa, by providing your banking information. To view the GitLab Services Attach Incentive Program, please visit the [GitLab Partner Portal](https://partners.gitlab.com/) and access the resources in the Program Documents folder in the Asset Library.

## Quote to Order

### Receive a GitLab Reseller Quote

Attached below is a sample quote. When you request a quote, you receive a document that looks very similar to this. This quote reflects your reseller price exclusive of incentive bonuses, and is not meant for the end customer. You will need to generate your own quote to the end customer.

The quote comes with a[ Sertifi](http://corp.sertifi.com/) link. E-signing the quote with Sertifi initiates the invoicing process and causes our systems to invoice you. Do not e-sign the quote until you are ready to be invoiced. Do not e-sign the quote if your customer will be paying us directly.

Note that we do not generate a quote, or fulfill an order, without an end user contact complete with email, shipping address, and postal code.

![alt_text](/images/Channel-Handbook2.png "image_tooltip")

## Receive a GitLab Reseller Quote via Distributor

### License Key Delivery

Once the order is invoiced and the customer accepts the EULA (see below), they are able to download their GitLab license key. Note, partners do not receive the license key, it is sent directly to the customer.

### Execute the GitLab EULA

All orders require an executed [EULA](https://about.gitlab.com/handbook/legal/subscription-agreement/) when there's a new subscription or an add-on. There are 2 methods of obtaining a EULA:

1. **License Key Deployment:** The default for reseller orders is that the end customer will receive a link to download their license key. The customer needs to click an acceptance of terms to get their key.
2. **Physical Signature:** Some customers may require a fully countersigned document.

An order is not complete without a signed agreement.



### Evaluation Licenses for Prospects

*   We will issue a 30-day evaluation license for your prospects if the deal is properly [registered](/handbook/resellers/#deal-registration).
*   We can renew this license if your customer needs more time.
*   Upon the second request for renewal (the third license), we assist you with a managed evaluation, where goals are set for the customer to meet. One of our Solutions Architects works with you and the prospect to bring them to completion before the third evaluation expires.

### Requesting a GitLab NRF (Not-for-Resale) License  

Authorized GitLab Partner fills out the NFR License Request Form which can be found on the [GitLab Partner Portal](https://partners.gitlab.com/English/) on the Services Page. 
    1. Partner Help desk (PHD) reviews the request and works with the partner if additional information is needed.
    2. Once activated the requestor will receive a license key via email.

### Not-for-Resale Program and Policy

The GitLab Not-for-Resale (NFR) Program offers qualified GitLab partners access to our solutions at no cost. The NFR Program provides partners with an opportunity to develop greater expertise on the GitLab platform. We encourage all GitLab partners to participate in the program and set up GitLab solutions. Consider using your environment to create integrations with GitLab Alliance Solution partners. This way your staff can fully understand the benefits and features of GitLab products and be better prepared to demonstrate products to customers.

Request your license by logging in to the [Partner Portal](https://www.partners.gitlab.com/); click the "Services" tab and the NFR Request form will be the first Quick Link. Please allow two (2) business days for processing your request.

* Partners can receive one Self-Managed and one Software-as-a-Service (SaaS) NFR license at the Ultimate level for a 12-month period.
    * Select partners can request a license for up to 25 users. Additional licenses may be available with business justification.
    * Open partners can request a license for up to 10 users.

Please allow two (2) business days for processing your request.

### NFR Program Eligibility

To be eligible for the NFR Program, partners must:
* Be Open or Select partners in good standing.
* Have at least one employee that has successfully completed the Solution Architect certification or Professional Services Engineer certification training, lab, and exam (with a passing score).

### NFR Renewals

GitLab NFR licenses expire after a 12-month subscription period, unless they are add-on licenses, in which case they expire with the partner’s oldest NFR licenses. 
* For Select partners in good standing, the licenses will be automatically renewed. 
* For Open partners in good standing, renewal must be requested.

### NFR Program Terms and Conditions

1. NFR software and services may be used solely and exclusively by the partner for the following purposes:
    * Internal employee training
    * Integration testing with related DevOps products and platforms
    * Partner-led product demonstrations to prospective customers
2. Partner in-house production use for customer engagements or internal development efforts requires purchased GitLab licenses, which are available to partners at a discount. Use of NFR licenses in a customer environment, including for managed services, is strictly prohibited. GitLab Channel Partners in compliance with the GitLab Partner Program are eligible for the NFR Program, but they must have at least one employee that has successfully completed the Solution Architect certification or Professional Services Engineer certification training, lab, and exam (with a passing score).
3. Select partners may request a license for up to 25 users. Additional licenses may be available with business justification and Open partners can request a license for up to 10 users without additional approval.
4. All software purchased under the NFR Program is subject to the terms and conditions of the GitLab End-User License Agreement at [https://about.gitlab.com/terms/](https://about.gitlab.com/terms/).
5. GitLab reserves the right to audit the use of NFR licenses to ensure they are in compliance with the NFR Program and reduce the number of licenses to a partner if they are not in compliance with the program.
6. GitLab reserves the right to change or cancel the NFR Program at any time and for any or no reason.
7. Partners can receive one Self-Managed and one SaaS NFR license at the Ultimate level for a 12-month period.

### NFR Support
[Support](https://about.gitlab.com/support) will be provided by the GitLab Support team. GitLab Solution Architects may also be available to help partners build out training, testing, and lab environments.

## Discounts and Referral Fees

The GitLab Partner Program helps develop your practice to best fit your business model. You can earn one-time and recurring revenues from product and services sales, referrals, and services delivery and resale.

To view the GitLab Partner Program Discount and Referral Fee Table, please visit the [GitLab Partner Portal](https://partners.gitlab.com/prm/English/s/assets) (must be an authorized partner) and access the Incentive Guide in the Program Documents folder in the Asset Library.

Authorized Public Sector partners can earn one-time and recurring revenues from product and services sales, referrals, and services delivery and resale. To view Public Sector discounts, please visit the [GitLab Partner Portal](https://partners.gitlab.com/prm/English/s/assets) (must be an authorized Public Sector partner) and access the Incentive Guide in the Program Documents folder in the Asset Library. GitLab employees can access the [discount table here](https://gitlab.my.salesforce.com/0694M00000DsShm?retUrl=%2F_ui%2Fcore%2Fchatter%2Ffiles%2FFileTabPage) and the [Public Sector table here](https://gitlab.my.salesforce.com/0694M00000DsShr?retUrl=%2F_ui%2Fcore%2Fchatter%2Ffiles%2FFileTabPage).

NOTE: Discounts are off list price. If GitLab is deeply discounting a large annual recurring revenue (ARR) customer engagement, the partner can reasonably expect to share in that with a discount reduction. The partner, GitLab sales, and Channel Account Manager must agree on the negotiated discount amount.

## Remitting payment to GitLab

You can arrange for payment either via invoice, or your customer can pay us directly.

### By Purchase Order

If your customer will be paying us via a Purchase Order, then you must email us a copy so that we may invoice the customer. Please send the Purchase Order to POfulfillment@gitlab.com for fulfillment.

### Paying via Invoice

To pay via invoice, simply e-sign the[ reseller quote](/handbook/resellers/#gitlab-quote) to initiate an invoice, then remit payment in USD to the bank listed on your quote.

## Definitions and Qualifications

* **Partner-Sourced/Initiated Discount –** Partners can earn the largest available product discount with a partner-sourced opportunity. This opportunity is a new opportunity for the GitLab Sales team, and is available to new or existing customers. 

The partner-sourced discount is an upfront discount. The partner is expected to assist the GitLab Sales team in closing the sale. To qualify for this discount, partners must submit a deal registration to GitLab via the GitLab Partner Portal, and it must be approved by GitLab Sales to qualify. A partner-sourced discount is available for both resale and referral opportunities. 

Partners purchasing GitLab for their own internal production use and add-on licenses at renewal can also qualify for a partner-sourced discount; however, they must first meet all program requirements for their specific program track (Open or Select). Please visit the Deal Registration Program guideline in the Program Guide for additional details.

* **Partner Co-Sell Opportunity –** Partners can earn a discount for a GitLab-sourced opportunity where the partner assists the GitLab Sales team in closing and/or transacting the deal. This may include demonstrating GitLab software, organizing executive meetings, supporting contract negotiations, delivering services, fulfillment, etc. It is an upfront discount. To qualify for the partner-assist discount, GitLab Sales team members will attach a partner to an opportunity, but partners do not need to submit a deal registration.

* **First Order Incentive –** Partners can receive an incentive paid as a rebate for partner-sourced opportunities for customers new to GitLab. The customer must be on the First Order customer target list, which is available from the GitLab Channel Sales team members. This incentive is only available for GitLab Select partners.

* **Services Attach Rebate –** Partners can receive an incentive paid as a rebate for partner-delivered services provided to end customers related to their use of their Premium and Ultimate GitLab software licenses. Applicable services are reviewed in the [Channel Services Catalog](https://about.gitlab.com/handbook/resellers/services/services-catalog/). The Services Attach incentive is based on a percentage of a customer's net annual recurring revenue (net ARR) of a linked software sale made within the last six (6) months. To reward partners that have made an investment in developing GitLab expertise, this incentive is only available to GitLab Professional Services and Training certified partners.

* **Referral Fees –** Referral fees are paid to partners for identifying new GitLab software sales opportunities that are not being resold by a partner. To qualify for a referral fee, partners must enter a referral fee deal registration in the GitLab Partner Portal. Each registration received by GitLab is considered a "Qualified Referral," provided that the referral is a GitLab sales opportunity that is new to GitLab, and the customer/prospect is willing to enter into a binding written agreement with GitLab to use GitLab products and services.

    * Opportunities identified by a Channel Partner but transacted through a Cloud Marketplace qualify for the referral fee.

    * All referral deal registrations must be approved by GitLab before becoming eligible for a referral fee. Referral fees are paid out no later than 45 days after the end of each quarter, if you have connected with GitLab in Coupa and provided your banking information.

* **Services Resale –** Partners can earn discounts on partner-sold services delivered by the GitLab Professional Services team. Partners qualify for a program discount on services resale if the services are included on the order of a deal registered opportunity. Partners are able to resell complete GitLab services offerings or include GitLab services as part of their own customer statement of work.

* **Subscription Renewals –** At the end of their GitLab license subscription, customers must be renewed to continue with the program subscription. At renewal, partners can earn a renewal discount for licenses renewed at the same subscription level, although no discounts are available for Starter/Bronze product tier renewals. If a customer adds additional licenses, those new licenses are discounted at the standard partner-sourced discount levels. If a customer upgrades their subscription level (e.g., Premium to Ultimate), the entire deal qualifies for the standard partner-sourced discount.

For customers whose most recent GitLab purchase was through a reseller partner, the incumbent reseller can earn a discount on the resale based on their program track, unless 1. the incumbent partner is no longer in compliance or 2. the customer provides a written request to GitLab to renew with a different partner. A deal registration on the original opportunity is not required for a partner to earn incumbent status.

Most customers grow their GitLab deployment over time. As such, renewals create additional opportunities for growth through:
* Upsell and expansion opportunities
* New integration, operational, and other professional services opportunities
* The sale and integration of GitLab Alliance Partner solutions
* Add-on sales of other non-GitLab products
* Subscription renewal discounts on future renewals

## Technical Support

While not required, we expect customers will, for the most part, contact you if they need help. It is in both of our best interests that they do so, as the more touch points you have with them, the more likely you are to further develop business with them. We do not expect you to be as knowledgeable about our products as our own support staff, and do expect that you may need to escalate some issues to our support staff.  For more information, visit the GitLab [Support](https://about.gitlab.com/support) page.

For pre-sales technical issues, please contact your local GitLab sales team.

## Channel Resources and Tools

We want to help our partners succeed. We have created resources and tools for our partner to help grow their GitLab business including: marketing development funds, instant marketing campaigns, microsites, press releases, case study tools, and more. [Learn more about what we offer.](/handbook/resellers/partner-resources-tools/)

Get started today by going to the [partner portal](https://partners.gitlab.com/English/). 

## Contact Us

All authorized GitLab resellers are invited to the GitLab #resellers Slack channel. This slack channel allows you to reach out to our sales and marketing team in a timely manner, as well as other resellers. Additionally, you can reach the GitLab channel team at partnersupport@gitlab.com and the GitLab marketing team at partner-marketing@gitlab.com. 
